use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long)]
    file: String,
}

mod bf {
    use std::fs::File;
    use std::io::prelude::*;

    pub struct BfMachine {
        inst_ptr: usize,
        mem_ptr: usize,
        mem: Vec<u8>,
        code: String,
        jump_table: Vec<(usize, usize)>, // pairs are the indexes of '[' and ']'
    }

    impl BfMachine {
        #[allow(unused)]
        pub fn parse_to_bf(file: String) -> String {
            let mut f = match File::open(file) {
                Ok(file) => file,
                Err(_) => {
                    panic!("Could not open the file. Is there even such a file?");
                }
            };
            let mut contents = String::new();
            f.read_to_string(&mut contents).unwrap();
            contents.retain(|c| matches!(c, '+' | '-' | '<' | '>' | '[' | ']' | ',' | '.'));
            contents
        }

        #[allow(unused)]
        pub fn from_file(file: String) -> BfMachine {
            let mem: Vec<u8> = vec![0; 30_000];
            let code = BfMachine::parse_to_bf(file);
            BfMachine {
                inst_ptr: 0,
                mem_ptr: 0,
                mem: mem,
                code: code,
                jump_table: vec![],
            }
        }

        #[allow(unused)]
        pub fn from_string(code: String) -> BfMachine {
            let mem: Vec<u8> = vec![0; 30_000];
            BfMachine {
                inst_ptr: 0,
                mem_ptr: 0,
                mem: mem,
                code: code,
                jump_table: vec![],
            }
        }

        pub fn interpret(&mut self) {
            let program_size = self.code.len();

            let mut brackets = Vec::new();
            for (i, chr) in self.code.chars().enumerate() {
                match chr {
                    '[' => brackets.push(i),
                    ']' => self
                        .jump_table
                        .push((brackets.pop().expect("Weird bracket stuff."), i)),
                    _ => continue,
                }
            }

            while self.inst_ptr != program_size {
                let chr = self.code.chars().nth(self.inst_ptr).unwrap();
                match chr {
                    '>' => {
                        if self.mem_ptr == 29_999 {
                            self.mem_ptr = 0
                        } else {
                            self.mem_ptr += 1;
                        }
                    }
                    '<' => {
                        if self.mem_ptr == 0 {
                            self.mem_ptr = 29_999;
                        } else {
                            self.mem_ptr -= 1;
                        }
                    }
                    '+' => self.mem[self.mem_ptr] += 1,
                    '-' => self.mem[self.mem_ptr] -= 1,
                    '[' => {
                        if self.mem[self.mem_ptr] == 0 {
                            self.inst_ptr = {
                                let mut result = None;
                                for &(k, v) in &self.jump_table {
                                    if self.inst_ptr == k {
                                        result = Some(v);
                                        break;
                                    }
                                }
                                result.expect("Mismatching brackets.")
                            }
                        }
                    }
                    ']' => {
                        if self.mem[self.mem_ptr] != 0 {
                            self.inst_ptr = {
                                let mut result = None;
                                for &(k, v) in &self.jump_table {
                                    if self.inst_ptr == v {
                                        result = Some(k);
                                        break;
                                    }
                                }
                                result.expect("Mismatching brackets.")
                            }
                        }
                    }
                    '.' => print!("{}", self.mem[self.mem_ptr] as char),
                    ',' => unsafe { self.mem[self.mem_ptr] = libc::getchar() as u8 },
                    _ => println!("wtf?"),
                }
                self.inst_ptr += 1;
            }
        }
    }
}

fn main() {
    let args = Args::parse();
    let mut makine = bf::BfMachine::from_file(args.file);
    makine.interpret();
}
